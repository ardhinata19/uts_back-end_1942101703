<?php

namespace Database\Seeders;

use App\Model\Song;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class SongSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        for ($i=0; $i < 50; $i++) {
            $data = [
                'title' => $faker->text(15),
                'year' => $faker->year,
                'artist' => $faker->streetName(),
                'gendre' => $faker->word,
                'duration' => $faker->randomFloat(4,2,4)
            ];
            Song::create($data);
        }
    }
}
