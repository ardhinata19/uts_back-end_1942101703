<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->post('api/uts/login', 'Auth\LoginController@verify');

$router->group(['prefix' => 'api/uts', 'middleware' => 'uts.auth'], function ($router) {
//  USER
    $router->group(['middleware' => 'uts.user'], function ($router) {
//     Playlist
        $router->get('/playlists', 'PlaylistController@ViewListPlaylist');
        $router->post('/playlists', 'PlaylistController@create');
        $router->get('/playlists/{id}', 'PlaylistController@ViewDetailPlaylist');
//  Song
        $router->get('/songs', 'SongController@getAllSong');
        $router->get('/songs/{id}', 'SongController@getSongById');
//  playlistsong
        $router->post('/playlists/{id}/songs', 'PlaylistSongController@addSong');
        $router->get('/playlists/{id}/songs', 'PlaylistSongController@getAllSongFromPlaylist');
    });

//  SUPERUSER
    $router->group(['middleware' => 'uts.superuser'], function ($router) {

//  Akses ke user
        $router->get('/users', 'SuperUserController@getAllUser');
        $router->get('/users/{id}', 'SuperUserController@getUserById');
        $router->post('/users', 'SuperUserController@createUser');
//  Song
        $router->post('/songs', 'SongController@createSong');
        $router->put('/songs/{id}', 'SongController@updateSong');
        $router->delete('/songs/{id}', 'SongController@deleteSong');
//  Playlist
        $router->get('/users/{id}/playlists', 'PlaylistController@getPlaylistByIdUser');

//  PlaylistSong
        $router->get('/users/{id}/playlists/{playlistId}/songs', 'PlaylistSongController@getSongByID_Playlist_User');
    });
});
