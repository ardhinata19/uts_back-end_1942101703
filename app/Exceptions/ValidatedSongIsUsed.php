<?php

namespace App\Exceptions;

use Exception;

class ValidatedSongIsUsed extends Exception
{
    public function render()
    {
        return response()->json([
            'status' => 'failed',
            'message' => 'Lagu Ini Sudah di Tambahkan ke Playlist'
        ], 403);
    }
}
