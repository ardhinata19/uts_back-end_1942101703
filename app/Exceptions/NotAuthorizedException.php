<?php

namespace App\Exceptions;

use Exception;

class NotAuthorizedException extends Exception
{
    public function render()
    {
        return response()->json([
            'status' => 'failed',
            'message' => 'Anda Tidak memiliki Hak Akses ke End-point ini'
        ], 403);
    }
}
