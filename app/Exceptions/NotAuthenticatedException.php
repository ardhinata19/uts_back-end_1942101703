<?php

namespace App\Exceptions;

use Exception;

class NotAuthenticatedException extends Exception
{
    public function render()
    {
        return response()->json([
            'status' => 'failed',
            'message' => 'Anda Tidak Terauntentikasi'
        ], 401);
    }
}
