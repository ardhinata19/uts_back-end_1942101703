<?php

namespace App\Exceptions;

use Exception;

class ValidatedNotUser extends Exception
{
    public function render()
    {
        return response()->json([
            'status' => 'failed',
            'message' => 'Anda Bukan Pemilik Playlist dengan ID Ini'
        ], 403);
    }
}
