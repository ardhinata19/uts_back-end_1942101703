<?php

namespace App\Http\Middleware;


use App\Exceptions\NotAuthenticatedException;

use App\Model\User;

use Closure;

class UtsMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (empty($request->header('api_token'))) {
            throw new NotAuthenticatedException();
        }
        $token = request()->header('api_token');
        $user = User::where('api_token', '=', $token)->first();
        if ($user == null) {
            throw new NotAuthenticatedException();
        }
        $request->user = $user;
        return $next($request);
    }
}
