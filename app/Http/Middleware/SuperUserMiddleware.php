<?php

namespace App\Http\Middleware;

use App\Exceptions\NotAuthorizedException;
use Closure;

class SuperUserMiddleware
{
    public function handle($request, Closure $next)
    {
        if ($request->user->role !== 'superuser') {
            throw new NotAuthorizedException();
        }
        return $next($request);
    }
}
