<?php

namespace App\Http\Middleware;

use App\Exceptions\NotAuthorizedException;
use Closure;

class UserMiddleware
{
    public function handle($request, Closure $next)
    {
        if ($request->user->role !== 'user') {
            throw new NotAuthorizedException();
        }
        return $next($request);
    }
}
