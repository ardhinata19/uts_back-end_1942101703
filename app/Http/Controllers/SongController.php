<?php


namespace App\Http\Controllers;


use App\Exceptions\ValidatedSongIsUsed;
use App\Http\Controllers\Base\UtsBaseController;
use App\Model\Playlist;
use App\Model\PlaylistSong;
use App\Model\Song;
use Illuminate\Support\Facades\Validator;
use phpDocumentor\Reflection\Types\False_;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use function PHPUnit\Framework\callback;

class SongController extends UtsBaseController
{
    public function getAllSong()
    {
        $songs = Song::all();
        return $this->successResponse(['songs' => $songs]);
    }

    public function getSongById($id)
    {
        $song = Song::find($id);
        if ($song == null) {
            throw new NotFoundHttpException();
        }
        return $this->successResponse(['song' => $song]);
    }

    public function createSong()
    {
        $validate = Validator::make(request()->all(), [
            'title' => 'required',
            'year' => 'required|numeric',
            'artist' => 'required',
            'gendre' => 'required',
            'duration' => 'required|numeric'
        ]);
        if ($validate->fails()) {
            return $this->failResponse($validate->errors()->getMessages(), 400);
        }
        $song = new Song();
        $song->title = request('title');
        $song->year = request('year');
        $song->artist = request('artist');
        $song->gendre = request('gendre');
        $song->duration = request('duration');
        $song->save();
        return $this->successResponse(['song' => $song], 201);
    }

    public function updateSong($id)
    {
        $song = Song::find($id);
        if ($song == null) {
            throw new NotFoundHttpException();
        }
        $validate = Validator::make(request()->all(), [
            'title' => 'required',
            'year' => 'required|numeric',
            'artist' => 'required',
            'gendre' => 'required',
            'duration' => 'required|numeric'
        ]);
        if ($validate->fails()) {
            return $this->failResponse($validate->errors()->getMessages(), 400);
        }
        $song = Song::find($id);
        $song->title = request('title');
        $song->year = request('year');
        $song->artist = request('artist');
        $song->gendre = request('gendre');
        $song->duration = request('duration');
        $song->save();
        return $this->successResponse(['song' => $song]);
    }

    public function deleteSong($id)
    {
        $song = Song::find($id);
        if ($song == null) {
            throw new NotFoundHttpException();
        }
        $songId = $song->id;
        $songValidated = PlaylistSong::all()->where('song_id', '=', $songId);

        if (!$songValidated->isEmpty()) {
            throw new ValidatedSongIsUsed();
        }
        $song->delete();
        return $this->successResponse(['song' => 'Lagu Berhasil di Hapus']);
    }
}
