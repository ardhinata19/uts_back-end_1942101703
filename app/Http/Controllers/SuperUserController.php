<?php


namespace App\Http\Controllers;


use App\Http\Controllers\Base\UtsBaseController;
use App\Model\User;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class SuperUserController extends UtsBaseController
{
    public function getAllUser()
    {
        $users = User::all();
        return $this->successResponse(['users'=>$users]);
    }

    public function getUserById($id)
    {
        $user = User::find($id);
        if($user == null) {
            throw new NotFoundHttpException();
        }
        return $this->successResponse(['user'=>$user]);
    }

    public function createUser()
    {
        $validate = Validator::make(request()->all(), [
            'email' => 'required',
            'password' => 'required',
            'role' => 'required',
            'fullname' => 'required',

        ]);
        if($validate->fails()){
            return $this->failResponse($validate->errors()->getMessages(),400);
        }
        $user = new User();
        $user->email = request('email');
        $user->password = request('password');
        $user['password'] = password_hash( $user['password'], PASSWORD_DEFAULT);
        $user->role = request('role');
        $user->fullname = request('fullname');
        $user->save();
        return $this->successResponse(['user'=>$user],201);
    }
}
