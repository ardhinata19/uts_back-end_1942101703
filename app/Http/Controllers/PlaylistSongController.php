<?php

namespace App\Http\Controllers;

use App\Exceptions\ValidatedNotUser;
use App\Http\Controllers\Base\UtsBaseController;
use App\Model\Playlist;
use App\Model\PlaylistSong;
use App\Model\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use function PHPUnit\Framework\isEmpty;

class PlaylistSongController extends UtsBaseController
{
    public function getAllSongFromPlaylist($id)
    {
        $playlist = Playlist::find($id);
        if ($playlist == null) {
            throw new NotFoundHttpException();
        }
        $userId = request()->user->id;
        $idUserPlaylist = $playlist->user_id;
        if ($userId !== $idUserPlaylist) {
            throw new ValidatedNotUser();
        }
        $playlistId = $playlist->id;
        $playlistSong = DB::table('songs')
            ->select('songs.id', 'title', 'year', 'artist', 'gendre', 'duration')
            ->join('playlistsongs', 'playlistsongs.song_id', '=', 'songs.id')
            ->where('playlistsongs.playlist_id', '=', $playlistId)
            ->get();

        return $this->validatedOwner(['Daftar Lagu' => $playlistSong]);
    }

    public function getSongByID_Playlist_User($id, $playlistId)
    {

        $user = User::find($id);
        $playlist = Playlist::find($playlistId);
        if ($user == null) {
            throw new NotFoundHttpException();
        }
        if ($playlist == null) {
            throw new NotFoundHttpException();
        }
        $userId = $user->id;
        $idPlaylist = $playlist->id;
        $idUserPlaylist = $playlist->user_id;

        if ($userId !== $idUserPlaylist) {
            throw new NotFoundHttpException();
        }
        $playlistSong = DB::table('songs')
            ->select('songs.id', 'title', 'year', 'artist', 'gendre', 'duration')
            ->join('playlistsongs', 'playlistsongs.song_id', '=', 'songs.id')
            ->where('playlistsongs.playlist_id', '=', $idPlaylist)
            ->get();

        return $this->successResponse(['Daftar Lagu' => $playlistSong]);
    }

    public function addSong($id)
    {
        $validate = Validator::make(request()->all(), [
            'song_id' => 'required'
        ]);
        if ($validate->fails()) {
            return $this->failResponse($validate->errors()->getMessages(), 400);
        }

        $idInput = Playlist::find($id);
        if ($idInput == null) {
            throw new NotFoundHttpException();
        }

        $playlistId = $idInput->id;
        if ($idInput->user_id !== request()->user->id) {
            throw new ValidatedNotUser();
        }
        $playlistsong = new PlaylistSong();
        $playlistsong->playlist_id = $playlistId;
        $playlistsong->song_id = request('song_id');

        $playlistsong->save();
        return $this->validatedOwner(['playlistsong' => 'Lagu berhasil ditambahkan ke playlist'], 201);
    }

}
