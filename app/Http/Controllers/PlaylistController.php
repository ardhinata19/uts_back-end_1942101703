<?php

namespace App\Http\Controllers;

use App\Exceptions\ValidatedNotUser;
use App\Http\Controllers\Base\UtsBaseController;
use App\Model\Playlist;
use App\Model\PlaylistSong;
use App\Model\User;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class PlaylistController extends UtsBaseController
{
    public function getPlaylistByIdUser($id)
    {
        $user = User::find($id);

        if ($user == null) {
            throw new NotFoundHttpException();
        }
        $userId = $user->id;
        $playlist = Playlist::all()->where('user_id', '=', $userId);
        return $this->successResponse(['playlist' => $playlist]);
    }

    public function ViewListPlaylist()
    {
        $userid = request()->user->id;
        $playlist = Playlist::all()
            ->where('user_id', '=', $userid);

        if ($playlist == false) {
            throw new NotFoundHttpException();
        }
        return $this->successResponse(['playlist' => $playlist]);
    }

    public function create()
    {
        $validate = Validator::make(request()->all(), [
            'name' => 'required'
        ]);
        if ($validate->fails()) {
            return $this->failResponse($validate->errors()->getMessages(), 400);
        }
        $playlist = new Playlist();
        $playlist->name = request('name');
        $playlist->user_id = request()->user->id;
        $playlist->save();
        return $this->successResponse(['playlist' => $playlist], 201);
    }

    public function ViewDetailPlaylist($id)
    {
        $playlist = Playlist::find($id);
        if ($playlist == null) {
            throw new NotFoundHttpException();
        }
        $idPlaylist = $playlist->user_id;
        $iduser = request()->user->id;
        if ($idPlaylist !== $iduser) {
            throw new ValidatedNotUser();
        }
        $play = PlaylistSong::all()
            ->where('playlist_id', '=', $playlist->id);
        return $this->successResponse(['playlist' => [$playlist, 'Daftar lagu' => $play]]);
    }
}
