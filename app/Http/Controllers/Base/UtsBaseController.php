<?php

namespace App\Http\Controllers\Base;

use App\Exceptions\NotAuthenticatedException;
use App\Model\User;
use Laravel\Lumen\Routing\Controller as BaseController;

abstract class UtsBaseController extends BaseController
{
    public function __construct()
    {
    }

    protected function successResponse(array $data, int $httpCode = 200)
    {
        $response = [
            'status' => 'succeed',
            'message' => 'Permintaan Berhasil di Proses',
            'data' => $data
        ];
        return response()->json($response, $httpCode);
    }

    protected function failResponse(array $data, int $httpCode)
    {
        $response = [
            'status' => 'failed',
            'message' => 'Permintaan Gagal di Proses',
            'data' => $data
        ];
        return response()->json($response, $httpCode);
    }

    protected function validatedOwner(array $data, int $httpCode = 200)
    {
        $response = [
            'status' => 'SUCCESS',
            'message' => 'Anda adalah pemilik playlist ini',
            'data' => $data
        ];
        return response()->json($response, $httpCode);
    }
}
